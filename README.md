We’re located in Soho on West Broadway between Broome and Grand. The Outpost is a spacious experience featuring a high-end barbershop, comfortable lounge, and retail experience. The elite BSG team of medical and grooming experts are available 7 days a week to help you look and feel your best.

Address: 345 W Broadway, New York, NY 10013, USA

Phone: 212-763-7716

Website: https://barbersurgeonsguild.com
